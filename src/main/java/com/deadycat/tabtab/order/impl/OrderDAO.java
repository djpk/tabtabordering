package com.deadycat.tabtab.order.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.deadycat.tabtab.order.OrderVO;
import com.deadycat.tabtab.order.OrderViewVO;

@Repository("orderDAO")
public class OrderDAO implements OrderService {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	// SQL 명령어들
	private final String ORDER_INSERT = "INSERT INTO `order`(id, shop_id, shoptable_id, status, tx_id, created_at) VALUES (?, ?, ?, ?, ?, ?)";
	//private final String ORDER_UPDATE = UPDATE ` SET ;
	private final String ORDER_DELETE = "UPDATE `order` SET status = 0 WHERE id = ?";
	private final String ORDER_GET = "SELECT * FROM `order` where id = ?";
	private final String ORDER_LIST = "SELECT o.id, o.shop_id, shoptable_id, name, o.status, created_at FROM `order` AS `o` JOIN `shoptable` AS st ON o.shoptable_id = st.id WHERE o.shop_id = ? AND o.status = 1 ORDER BY created_at DESC";

	// CRUD 기능의 메소드 구현
	// 주문 등록
	public void insertOrder(OrderVO vo) {
		System.out.println("===> JDBC로 insertOrder() 처리");
		jdbcTemplate.update(ORDER_INSERT, vo.getId(), vo.getShopId(), vo.getShoptableId(), vo.getStatus(), vo.getTxId(), vo.getCreatedAt());
	}
	
	// 주문 수정
	public void updateOrder(OrderVO vo) {
		System.out.println("===> JDBC로 updateOrder() 처리 ");
	}
	
	// 주문 삭제 (종결처리)
	public void deleteOrder(OrderVO vo) {
		System.out.println("===> JDBC로 deleteOrder() 처리");
		jdbcTemplate.update(ORDER_DELETE, vo.getId());
	}
	
	// 숍 조회
	public OrderVO getOrder(OrderVO vo) {
		System.out.println("===> JDBC로 getOrder() 처리");
		return jdbcTemplate.queryForObject(ORDER_GET, new Object[] {vo.getId()},
				(rs, rowNum) -> new OrderVO(rs.getString("id"), rs.getString("shop_id"), rs.getString("shoptable_id"), rs.getInt("status"), rs.getString("tx_id"), rs.getString("created_at")));
	}
	
	// 숍 목록 조회
	public List<OrderViewVO> getOrderList(String shopId) {
		System.out.println("===> JDBC로 getOrderList() 처리");
		return jdbcTemplate.query(ORDER_LIST, new Object[] {shopId},
				(rs, rowNum) -> new OrderViewVO(rs.getString("id"), rs.getString("shop_id"), rs.getString("name"), rs.getInt("status"), rs.getString("created_at")));
	}
}
