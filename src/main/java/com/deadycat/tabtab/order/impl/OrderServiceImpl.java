package com.deadycat.tabtab.order.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deadycat.tabtab.order.OrderVO;
import com.deadycat.tabtab.order.OrderViewVO;

@Service("orderService")
public class OrderServiceImpl implements OrderService {
	@Autowired
	private OrderDAO orderDAO;
	
	public void insertOrder(OrderVO vo) {
		// TODO Auto-generated method stub
		orderDAO.insertOrder(vo);	
	}

	public void updateOrder(OrderVO vo) {
		// TODO Auto-generated method stub
		orderDAO.updateOrder(vo);
	}

	public void deleteOrder(OrderVO vo) {
		// TODO Auto-generated method stub
		orderDAO.deleteOrder(vo);
	}

	public OrderVO getOrder(OrderVO vo) {
		// TODO Auto-generated method stub
		return orderDAO.getOrder(vo);
	}

	public List<OrderViewVO> getOrderList(String shopId) {
		// TODO Auto-generated method stub
		return orderDAO.getOrderList(shopId);
	}

}
