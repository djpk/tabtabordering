package com.deadycat.tabtab.order.impl;

import java.util.List;

import com.deadycat.tabtab.order.OrderVO;
import com.deadycat.tabtab.order.OrderViewVO;

public interface OrderService {

	// CRUD 기능의 메소드 구현
	// 오더 등록
	void insertOrder(OrderVO vo);

	// 오더 수정
	void updateOrder(OrderVO vo);

	// 오더 삭제
	void deleteOrder(OrderVO vo);

	// 오더 조회
	OrderVO getOrder(OrderVO vo);

	// 오더 목록 조회
	List<OrderViewVO> getOrderList(String shopId);

}