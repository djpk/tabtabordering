package com.deadycat.tabtab.order;

public class OrderViewVO {
	private String id;
	private String shopId;
	private String shoptableName;
	private int status;
	private String createdAt;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getShopId() {
		return shopId;
	}
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	public String getShoptableName() {
		return shoptableName;
	}
	public void setShoptableName(String shoptableName) {
		this.shoptableName = shoptableName;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}
	public OrderViewVO(String id, String shopId, String shoptableName, int status, String createdAt) {
		super();
		this.id = id;
		this.shopId = shopId;
		this.shoptableName = shoptableName;
		this.status = status;
		this.createdAt = createdAt;
	}
	
}
