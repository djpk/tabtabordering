package com.deadycat.tabtab.order;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class OrderVO {
	private String id;
	private String shopId;
	private String shoptableId;
	private int status;
	private String txId;
	private LocalDateTime createdAt;
	public OrderVO() { }
	public OrderVO(String id, String shopId, String shoptableId, int status, String txId, String createdAt) {
		super();
		this.id = id;
		this.shopId = shopId;
		this.shoptableId = shoptableId;
		this.status = status;
		this.txId = txId;
		this.setCreatedAt(createdAt);
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getShopId() {
		return shopId;
	}
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	public String getShoptableId() {
		return shoptableId;
	}
	public void setShoptableId(String shoptableId) {
		this.shoptableId = shoptableId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getTxId() {
		return txId;
	}
	public void setTxId(String txId) {
		this.txId = txId;
	}
	public String getCreatedAt() {
		return createdAt.toString();
	}
	public void setCreatedAt() {
		this.createdAt = LocalDateTime.now();
	}
	public void setCreatedAt(String time) {
		this.createdAt = LocalDateTime.parse(time, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
	}
	@Override
	public String toString() {
		return "OrderVO [id=" + id + ", shopId=" + shopId + ", shoptableId=" + shoptableId + ", status=" + status
				+ ", txId=" + txId + ", createdAt=" + createdAt + "]";
	}

}
