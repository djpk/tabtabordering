package com.deadycat.tabtab.common;

import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class QrcodeHandler {
	public static void getQR(String shoptableId) {
		try {
			File f = new File("/opt/tomcat/webapps/tabtab/qrcodes/" + shoptableId + ".png");
			
			if (!f.exists()) {
				String url = "http://13.209.229.109:8080/tabtab/order/" + shoptableId;
				QRCodeWriter writer = new QRCodeWriter();
						
				BitMatrix matrix = writer.encode(url, BarcodeFormat.QR_CODE, 256, 256); 
				MatrixToImageConfig config = new MatrixToImageConfig(0xFF000000,0xFFFFFFFF);
				BufferedImage qrImage = MatrixToImageWriter.toBufferedImage(matrix,config);
				ImageIO.write(qrImage, "png", new File("/opt/tomcat/webapps/tabtab/qrcodes/" + shoptableId + ".png"));	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
