package com.deadycat.tabtab.common;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Service;

@Service
@Aspect
public class TestLogger {
	@Pointcut("execution(* com.deadycat.tabtab..*Impl.*(..))")
	public void pt() {}
	
	@Before("pt()")
	public void getLogger() {
		System.out.println("===== 로깅 시작! =====");
	}
}
