package com.deadycat.tabtab.detailorder.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.deadycat.tabtab.detailorder.DetailOrderJoinVO;
import com.deadycat.tabtab.detailorder.DetailOrderVO;
import com.deadycat.tabtab.detailorder.DetailOrderViewVO;

@Repository("detailOrderDAO")
public class DetailOrderDAO implements DetailOrderService {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	// SQL 명령어들
	private final String DETAILORDER_INSERT = "INSERT INTO `detailorder`(id, order_id, product_id, price, quantity) VALUES (?, ?, ?, ?, ?)";
	//private final String USER_UPDATE = UPDATE user SET ;
	private final String DETAILORDER_DELETE = "DELETE detailorder WHERE id = ?";
	private final String DETAILORDER_GET = "SELECT * FROM `detailorder` where id = ?";
	private final String DETAILORDER_LIST = "SELECT p.name, o.quantity FROM `detailorder` AS o JOIN `product` AS p ON p.id = o.product_id WHERE order_id = ?";

	// CRUD 기능의 메소드 구현
	// 세부 오더 등록
	public void insertDetailOrder(DetailOrderVO vo) {
		System.out.println("===> JDBC로 insertDetailOrder() 처리");
		jdbcTemplate.update(DETAILORDER_INSERT, vo.getId(), vo.getOrderId(), vo.getProductId(), vo.getPrice(), vo.getQuantity());
	}
	
	// 세부 오더 수정
	public void updateDetailOrder(DetailOrderVO vo) {
		System.out.println("===> JDBC로 updateDetailOrder() 처리 ");
	}
	
	// 세부 오더 삭제
	public void deleteDetailOrder(DetailOrderVO vo) {
		System.out.println("===> JDBC로 deleteDetailOrder() 처리");
		jdbcTemplate.update(DETAILORDER_DELETE, vo.getId());
	}
	
	// 세부 오더 조회
	public DetailOrderVO getDetailOrder(DetailOrderVO vo) {
		System.out.println("===> JDBC로 getDetailOrder() 처리");
		return jdbcTemplate.queryForObject(DETAILORDER_GET, new Object[] {vo.getId()},
				(rs, rowNum) -> new DetailOrderVO(rs.getString("id"), rs.getString("order_id"), rs.getString("product_id"), rs.getDouble("price"), rs.getInt("quantity")));
	}
	
	// 세부 오더 목록 조회
	public List<DetailOrderJoinVO> getDetailOrderList(String orderId) {
		System.out.println("===> JDBC로 getDetailOrderList() 처리");
		return jdbcTemplate.query(DETAILORDER_LIST, new Object[] {orderId},
				(rs, rowNum) -> new DetailOrderJoinVO(rs.getString("name"), rs.getInt("quantity")));
	}
}