package com.deadycat.tabtab.detailorder.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deadycat.tabtab.detailorder.DetailOrderJoinVO;
import com.deadycat.tabtab.detailorder.DetailOrderVO;

@Service("detailOrderService")
public class DetailOrderImpl implements DetailOrderService {
	@Autowired
	private DetailOrderDAO detailOrderDAO;
	
	public void insertDetailOrder(DetailOrderVO vo) {
		// TODO Auto-generated method stub
		detailOrderDAO.insertDetailOrder(vo);
	}

	public void updateDetailOrder(DetailOrderVO vo) {
		// TODO Auto-generated method stub
		detailOrderDAO.updateDetailOrder(vo);
	}

	public void deleteDetailOrder(DetailOrderVO vo) {
		// TODO Auto-generated method stub
		detailOrderDAO.deleteDetailOrder(vo);
	}

	public DetailOrderVO getDetailOrder(DetailOrderVO vo) {
		// TODO Auto-generated method stub
		return detailOrderDAO.getDetailOrder(vo);
	}

	public List<DetailOrderJoinVO> getDetailOrderList(String orderId) {
		// TODO Auto-generated method stub
		return detailOrderDAO.getDetailOrderList(orderId);
	}

}
