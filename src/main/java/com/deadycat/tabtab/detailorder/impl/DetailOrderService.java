package com.deadycat.tabtab.detailorder.impl;

import java.util.List;

import com.deadycat.tabtab.detailorder.DetailOrderJoinVO;
import com.deadycat.tabtab.detailorder.DetailOrderVO;

public interface DetailOrderService {

	// CRUD 기능의 메소드 구현
	// 세부오더 등록
	void insertDetailOrder(DetailOrderVO vo);

	// 세부 오더 수정
	void updateDetailOrder(DetailOrderVO vo);

	// 세부 오더 삭제
	void deleteDetailOrder(DetailOrderVO vo);

	// 세부 오더 조회
	DetailOrderVO getDetailOrder(DetailOrderVO vo);

	// 세부 오더 목록 조회
	List<DetailOrderJoinVO> getDetailOrderList(String orderId);

}