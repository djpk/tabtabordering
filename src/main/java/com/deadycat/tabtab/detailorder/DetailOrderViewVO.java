package com.deadycat.tabtab.detailorder;

public class DetailOrderViewVO {
	private String orderId; //OrderViewVO
	private String shoptableName; //OrderViewVO
	private String productName; // DetailOrderJoinVO
	private int quantity; // DetailOrderJoinVO
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getShoptableName() {
		return shoptableName;
	}
	public void setShoptableName(String shoptableName) {
		this.shoptableName = shoptableName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public DetailOrderViewVO(String orderId, String shoptableName, String productName, int quantity) {
		super();
		this.orderId = orderId;
		this.shoptableName = shoptableName;
		this.productName = productName;
		this.quantity = quantity;
	}
	public DetailOrderViewVO() {
		// TODO Auto-generated constructor stub
	}
	
}
