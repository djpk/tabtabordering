package com.deadycat.tabtab.detailorder;

public class DetailOrderJoinVO {
	private String productName;
	private int quantity;
	public DetailOrderJoinVO(String productName, int quantity) {
		super();
		this.productName = productName;
		this.quantity = quantity;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
