package com.deadycat.tabtab.product;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ProductVO {
	private String id;
	private String name;
	private int price;
	private String shopId;
	private String imageUrl;
	private LocalDateTime createdAt;
	private int status;
	private int weight;
	
	public ProductVO() { }
	public ProductVO(String id, String name, int price, String shopId, String imageUrl, String createdAt, int status, int weight) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.shopId = shopId;
		this.imageUrl = imageUrl;
		this.setCreatedAt(createdAt);
		this.status = status;
		this.weight = weight;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getShopId() {
		return shopId;
	}
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public String getCreatedAt() {
		return createdAt.toString();
	}
	public void setCreatedAt() {
		this.createdAt = LocalDateTime.now();
	}
	public void setCreatedAt(String time) {
		this.createdAt = LocalDateTime.parse(time, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	@Override
	public String toString() {
		return "ProductVO [id=" + id + ", name=" + name + ", price=" + price + ", shopId=" + shopId + ", imageUrl="
				+ imageUrl + ", createdAt=" + createdAt + "]";
	}
	
	
}
