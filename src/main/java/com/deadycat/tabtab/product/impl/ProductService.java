package com.deadycat.tabtab.product.impl;

import java.util.List;

import com.deadycat.tabtab.product.ProductVO;

public interface ProductService {

	// CRUD 기능의 메소드 구현
	// 상품 등록
	void insertProduct(ProductVO vo);

	// 상품 수정
	void updateProduct(ProductVO vo);

	// 상품 삭제
	void deleteProduct(ProductVO vo);

	// 상품 조회
	ProductVO getProduct(ProductVO vo);

	// 상품 목록 조회
	List<ProductVO> getProductList(String shopId);

}