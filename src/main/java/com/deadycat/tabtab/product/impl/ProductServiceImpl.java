package com.deadycat.tabtab.product.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deadycat.tabtab.product.ProductVO;

@Service("productService")
public class ProductServiceImpl implements ProductService {
	@Autowired
	private ProductDAO productDAO;
	
	public void insertProduct(ProductVO vo) {
		// TODO Auto-generated method stub
		productDAO.insertProduct(vo);
	}

	public void updateProduct(ProductVO vo) {
		// TODO Auto-generated method stub
		productDAO.updateProduct(vo);
	}

	public void deleteProduct(ProductVO vo) {
		// TODO Auto-generated method stub
		productDAO.deleteProduct(vo);
	}

	public ProductVO getProduct(ProductVO vo) {
		// TODO Auto-generated method stub
		return productDAO.getProduct(vo);
	}

	public List<ProductVO> getProductList(String shopId) {
		// TODO Auto-generated method stub
		return productDAO.getProductList(shopId);
	}

}
