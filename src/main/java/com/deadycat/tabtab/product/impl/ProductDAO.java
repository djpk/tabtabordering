package com.deadycat.tabtab.product.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.deadycat.tabtab.product.ProductVO;

@Repository("productDAO")
public class ProductDAO implements ProductService {
		@Autowired
		private JdbcTemplate jdbcTemplate;
		
		// SQL 명령어들
		private final String PRODUCT_INSERT = "INSERT INTO `product`(id, name, price, shop_id, image_url, created_at, status, weight) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		private final String PRODUCT_UPDATE = "UPDATE product SET weight = ?, price = ? WHERE id = ?";
		private final String PRODUCT_DELETE = "UPDATE product SET status = 0 WHERE id = ?";
		private final String PRODUCT_GET = "SELECT * FROM `product` where id = ?";
		private final String PRODUCT_LIST = "SELECT * FROM `product` WHERE shop_id = ? AND status = 1 ORDER BY weight ASC";		
		
		// CRUD 기능의 메소드 구현
		// 상품 등록
		public void insertProduct(ProductVO vo) {
			System.out.println("===> JDBC로 insertProduct() 처리");
			jdbcTemplate.update(PRODUCT_INSERT, vo.getId(), vo.getName(), vo.getPrice(), vo.getShopId(), vo.getImageUrl(), vo.getCreatedAt(), 1, vo.getWeight());
		}
		
		// 상품 수정
		public void updateProduct(ProductVO vo) {
			System.out.println("===> JDBC로 updateProduct() 처리 ");
			jdbcTemplate.update(PRODUCT_UPDATE, vo.getWeight(), vo.getPrice(), vo.getId());
		}
		
		// 상품 삭제
		public void deleteProduct(ProductVO vo) {
			System.out.println("===> JDBC로 deleteProduct() 처리");
			jdbcTemplate.update(PRODUCT_DELETE, vo.getId());
		}
		
		// 상품 조회
		public ProductVO getProduct(ProductVO vo) {
			System.out.println("===> JDBC로 getProduct() 처리");
			return jdbcTemplate.queryForObject(PRODUCT_GET, new Object[] {vo.getId()},
					(rs, rowNum) -> new ProductVO(rs.getString("id"), rs.getString("name"), rs.getInt("price"), rs.getString("shop_id"), rs.getString("image_url"), rs.getString("created_at"), rs.getInt("status"), rs.getInt("weight")));
		}
		
		// 상품 목록 조회
		public List<ProductVO> getProductList(String shopId) {
			System.out.println("===> JDBC로 getProductList() 처리");
			return jdbcTemplate.query(PRODUCT_LIST, new Object[] {shopId}, (rs, rowNum) -> new ProductVO(rs.getString("id"), rs.getString("name"), rs.getInt("price"), rs.getString("shop_id"), rs.getString("image_url"), rs.getString("created_at"), rs.getInt("status"), rs.getInt("weight")));
		}
		
}
