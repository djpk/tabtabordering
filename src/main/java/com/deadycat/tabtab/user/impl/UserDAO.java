package com.deadycat.tabtab.user.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.deadycat.tabtab.common.JdbcHandler;
import com.deadycat.tabtab.user.UserVO;

@Repository("userDAO")
public class UserDAO implements UserService {
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	// JDBC 관련 변수
//	private Connection conn = null;
//	private PreparedStatement stmt = null;
//	private ResultSet rs = null;
	
	// SQL 명령어들
	private final String USER_INSERT = "INSERT INTO user(id, login_id, password, email, name, created_at) VALUES (?, ?, ?, ?, ?, ?)";
	//private final String USER_UPDATE = UPDATE user SET ;
	private final String USER_DELETE = "DELETE user WHERE id = ?";
	private final String USER_GET = "SELECT * FROM user where id = ?";
	private final String USER_LIST = "SELECT * FROM user ORDER BY id DESC";

	// CRUD 기능의 메소드 구현
	// 사용자 등록
	public void insertUser(UserVO vo) {
		System.out.println("===> JDBC로 insertUser() 처리");
		jdbcTemplate.update(USER_INSERT, vo.getId(), vo.getLoginId(), vo.getPassword(), vo.getEmail(), vo.getName(), vo.getCreatedAt());
		
		//TX는 메소드 단위로 진행된다.
		//한 메소드안에서 에러나면 롤백됨
//		for(int i=0; i<100; i++) {
//			if (i == 64)
//				throw new IllegalArgumentException("67번은 사용할 수 없습니다.");
//			
//			jdbcTemplate.update(USER_INSERT, String.valueOf(i), vo.getLoginId(), vo.getPassword(), vo.getEmail(), vo.getName(), vo.getCreatedAt());
//			System.out.println("===> " + i + " 처리");
//		}
		
//		try {
//			conn = JdbcHandler.getConnection();
//			stmt = conn.prepareStatement(USER_INSERT);
//			stmt.setString(1, vo.getId());
//			stmt.setString(2, vo.getLoginId());
//			stmt.setString(3, vo.getPassword());
//			stmt.setString(4, vo.getEmail());
//			stmt.setString(5, vo.getName());
//			stmt.setString(6, vo.getCreatedAt());
//			stmt.executeUpdate();
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			JdbcHandler.close(stmt, conn);
//		}
	}
	
	// 글 수정
	public void updateUser(UserVO vo) {
		System.out.println("===> JDBC로 updateUser() 처리 ");
	}
	
	public void deleteUser(UserVO vo) {
		System.out.println("===> JDBC로 deleteUser() 처리");
	}
	
	public UserVO getUser(UserVO vo) {
		System.out.println("===> JDBC로 getUser() 처리");
		Object[] args = {vo.getId()};
		return jdbcTemplate.queryForObject(USER_GET, args, new UserRowMapper());
		//		UserVO user = null;
//		try {
//			conn = JdbcHandler.getConnection();
//			stmt = conn.prepareStatement(USER_GET);
//			stmt.setString(1, vo.getId());
//			rs = stmt.executeQuery();
//
//			if (rs.next()) {
//				user = new UserVO();
//				user.setId(rs.getString("id"));
//				user.setLoginId(rs.getString("login_id"));
//				user.setPassword(rs.getString("password"));
//				user.setEmail(rs.getString("email"));
//				user.setName(rs.getString("name"));
//				user.setCreatedAt(rs.getString("created_at"));
//			}
//			
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			JdbcHandler.close(stmt, conn);
//		}
//		return user;
	}
	
	public List<UserVO> getUserList(UserVO vo) {
		System.out.println("===> JDBC로 getUserList() 처리");
		return null;
	}
}

class UserRowMapper implements RowMapper<UserVO> {
	public UserVO mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		UserVO user = new UserVO();
		user.setId(rs.getString("id"));
		user.setLoginId(rs.getString("login_id"));
		user.setPassword(rs.getString("password"));
		user.setEmail(rs.getString("email"));
		user.setName(rs.getString("name"));
		user.setCreatedAt(rs.getString("created_at"));
		return user;
	}
}