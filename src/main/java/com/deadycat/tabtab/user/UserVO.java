package com.deadycat.tabtab.user;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class UserVO {
	private String id;
	private String loginId;
	private String password;
	private String email;
	private String name;
	private LocalDateTime createdAt;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCreatedAt() {
		return createdAt.toString();
	}
	public void setCreatedAt() {
		this.createdAt = LocalDateTime.now();
	}
	public void setCreatedAt(String time) {
		this.createdAt = LocalDateTime.parse(time, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
	}
	@Override
	public String toString() {
		return "UserVO [id=" + id + ", loginId=" + loginId + ", password=" + password + ", email=" + email + ", name="
				+ name + ", createdAt=" + createdAt + "]";
	}
	
}
