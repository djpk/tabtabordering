package com.deadycat.tabtab.view.controller;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.deadycat.tabtab.product.ProductVO;
import com.deadycat.tabtab.product.impl.ProductService;

@Controller
public class ProductController {
	@Autowired
	ProductService productService;
	
	@RequestMapping(value="/product", method=RequestMethod.GET)
	public ModelAndView productPage(ModelAndView mav, HttpSession session) {
		if (session.getAttribute("shop") != null) {
			// 현재 SHOP ID로 PRODUCT전부 조회해서 보여주기
			String shopId = (String) session.getAttribute("shop");
			List<ProductVO> d = productService.getProductList(shopId);
			System.out.println(d.toString());
			mav.addObject("productList", d);
			mav.setViewName("product.jsp");
		} else {
			mav.addObject("msg", "매장 정보가 없습니다. 매장을 먼저 생성해주세요.");
			mav.setViewName("error.jsp");
		}
		return mav;
	}
	
	@RequestMapping(value="/productAdd", method=RequestMethod.GET)
	public ModelAndView productAddPage(ModelAndView mav) {
		mav.setViewName("register_product.jsp");
		return mav;
	}
	
	@RequestMapping(value="/product", method=RequestMethod.POST)
	public ModelAndView productAdd(@RequestParam(value="productName") String name, @RequestParam(value="productPrice") String price, @RequestParam(value="productWeight") String weight, ModelAndView mav, HttpSession session) {
		if (session.getAttribute("shop") != null) {
			ProductVO product = new ProductVO();
			product.setId(UUID.randomUUID().toString().replace("-", ""));
			product.setCreatedAt();
			product.setShopId((String) session.getAttribute("shop"));
			product.setName(name);
			product.setStatus(1);
			product.setImageUrl("null");
			product.setPrice(Integer.parseInt(price));
			product.setWeight(Integer.parseInt(weight));
			productService.insertProduct(product);
		}
		mav.setViewName("redirect:/product");
		return mav;
	}
	
	@RequestMapping(value="/product", method=RequestMethod.PUT)
	public ModelAndView productUpdate(@RequestParam(value="productId") String id, @RequestParam(value="productPrice") String price, @RequestParam(value="productWeight") String weight, ModelAndView mav, HttpSession session) {
		if (session.getAttribute("shop") != null) {
			ProductVO vo = new ProductVO();
			vo.setId(id);
			vo.setPrice(Integer.parseInt(price));
			vo.setWeight(Integer.parseInt(weight));
			productService.updateProduct(vo);
		}
		mav.setViewName("redirect:/product");
		return mav;
	}
	
	@RequestMapping(value="/product/delete/{productId}", method=RequestMethod.GET)
	public ModelAndView productDelete(ProductVO vo, ModelAndView mav, @PathVariable String productId) {
		vo.setId(productId);
		productService.deleteProduct(vo);
		mav.setViewName("redirect:/product");
		return mav;
	}
	
	@RequestMapping(value="/product/update/{productId}", method=RequestMethod.GET)
	public ModelAndView productUpdatePage(ProductVO vo, ModelAndView mav, @PathVariable String productId) {
		mav.addObject("productId", productId);
		mav.setViewName("/update_product.jsp");
		return mav;
	}
}
