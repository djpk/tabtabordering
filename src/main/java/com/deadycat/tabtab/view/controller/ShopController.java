package com.deadycat.tabtab.view.controller;

import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.deadycat.tabtab.shop.ShopVO;
import com.deadycat.tabtab.shop.impl.ShopService;
import com.deadycat.tabtab.shoptable.ShopTableVO;
import com.deadycat.tabtab.shoptable.impl.ShopTableService;

@Controller
public class ShopController {
	@Autowired
	private ShopService shopService;
	@Autowired
	private ShopTableService shoptableService;
	
	@RequestMapping(value="/shop", method=RequestMethod.GET)
	public ModelAndView shopPage(ShopVO vo, ModelAndView mav, HttpSession session) {
		if (session.getAttribute("user") == null) {
			//mav.addObject("msg", "사용자 정보가 없습니다. 다시 로그인해주세요.");
			mav.setViewName("redirect:/login");
			return mav;
		}
		
		if (session.getAttribute("shop") == null) {
			// 매장 등록으로 이동합니다.
			mav.setViewName("register_shop.jsp");
			return mav;
		}
		
		// ShopId로 Shoptable 조회하기
		String shopId = (String) session.getAttribute("shop");
		List<ShopTableVO> d = shoptableService.getShopTableList(shopId);
		
		System.out.println(d.toString());
		mav.addObject("shoptableList", d);
		mav.setViewName("shop.jsp");
		
		return mav;
	}
	
	@RequestMapping(value="/shoptable", method=RequestMethod.GET)
	public ModelAndView shoptableAddPage(ModelAndView mav, HttpSession session) {
		if (session.getAttribute("user") == null) {
			//mav.addObject("msg", "사용자 정보가 없습니다. 다시 로그인해주세요.");
			mav.setViewName("redirect:/login");
			return mav;
		}
		mav.setViewName("register_shoptable.jsp");
		return mav;
	}
	
	@RequestMapping(value="/shoptable", method=RequestMethod.POST)
	public String registerShoptable(@RequestParam(value="shoptableName") String name, ModelAndView mav, HttpSession session) {
		if (session.getAttribute("shop") != null) {
			ShopTableVO vo = new ShopTableVO();
			vo.setId(UUID.randomUUID().toString().replace("-", ""));
			vo.setShopId((String) session.getAttribute("shop"));
			vo.setName(name);
			vo.setStatus(1);
			vo.setPosX(0);
			vo.setPosY(0);
			vo.setWidth(0);
			vo.setHeight(0);
			System.out.println(vo.toString());
			shoptableService.insertShopTable(vo);
		}
		return "redirect:/shop";
	}
	
	@RequestMapping(value="/shoptable/delete/{shoptableId}", method=RequestMethod.GET)
	public ModelAndView productAddPage(ShopTableVO vo, ModelAndView mav, @PathVariable String shoptableId) {
		vo.setId(shoptableId);
		shoptableService.deleteShopTable(vo);
		mav.setViewName("redirect:/shop");
		return mav;
	}
	
	@RequestMapping(value="/shop", method=RequestMethod.POST)
	public ModelAndView registerShop(@RequestParam(value="shopName") String name, ModelAndView mav, HttpSession session) { // 매개변수로 선언하면 Spring 컨테이너가 알아서 객체 생성해줌
		
		System.out.println("입력하신 이름 : " + name);
		ShopVO shop = new ShopVO();
		shop.setId(UUID.randomUUID().toString().replace("-", ""));
		shop.setCreatedAt();
		shop.setUserId((String) session.getAttribute("user"));
		shop.setName(name);
		
		shopService.insertShop(shop);
		
		ShopVO newShop = shopService.getShop(shop);
		if (newShop != null) {
			session.setAttribute("shop", newShop.getId());
			mav.addObject("shop", newShop);
			mav.setViewName("shop.jsp");
		} else {
			mav.addObject("msg", "매장 생성 중 오류가 발생하였습니다.");
			mav.setViewName("error.jsp");
		}
		
		return mav;
	}
}