package com.deadycat.tabtab.view.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.deadycat.tabtab.shoptable.ShopTableVO;
import com.deadycat.tabtab.shoptable.impl.ShopTableService;

import static com.deadycat.tabtab.common.QrcodeHandler.getQR;

@Controller
public class QrcodeController {
	@Autowired
	ShopTableService shoptableService;
	
	@RequestMapping(value="/qrcode", method=RequestMethod.GET)
	public ModelAndView getCode(ModelAndView mav, HttpSession session) {
		if (session.getAttribute("user") != null) {
			if (session.getAttribute("shop") != null) {
				
				List<ShopTableVO> list = shoptableService.getShopTableList((String) session.getAttribute("shop"));
				
				for (ShopTableVO item : list) {
					getQR(item.getId());
				}
				
				mav.addObject("shoptableList", list);
				mav.setViewName("qrcode.jsp");
				
			} else {
				mav.addObject("msg", "매장 정보가 없습니다. 매장을 먼저 생성해주세요.");
				mav.setViewName("error.jsp");
			}
		} else {
			mav.setViewName("redirect:/login");
		}
		return mav;
	}
}
