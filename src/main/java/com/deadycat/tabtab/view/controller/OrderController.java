package com.deadycat.tabtab.view.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.converter.StringMessageConverter;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.deadycat.tabtab.detailorder.DetailOrderJoinVO;
import com.deadycat.tabtab.detailorder.DetailOrderVO;
import com.deadycat.tabtab.detailorder.DetailOrderViewVO;
import com.deadycat.tabtab.detailorder.impl.DetailOrderService;
import com.deadycat.tabtab.order.OrderVO;
import com.deadycat.tabtab.order.OrderViewVO;
import com.deadycat.tabtab.order.impl.OrderService;
import com.deadycat.tabtab.product.ProductVO;
import com.deadycat.tabtab.product.impl.ProductService;
import com.deadycat.tabtab.shoptable.ShopTableVO;
import com.deadycat.tabtab.shoptable.impl.ShopTableService;

@Controller
public class OrderController {
	@Autowired
	private OrderService orderService;
	@Autowired
	private DetailOrderService detailOrderService;
	@Autowired
	private ProductService productService;
	@Autowired
	private ShopTableService shoptableService;
	@Autowired
	private SimpMessagingTemplate msgtmp;
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String goOrder() {
		return "redirect:/order";
	}
	
	// 상점 페이지 (관리자용)
	@RequestMapping(value="/order", method=RequestMethod.GET)
	public ModelAndView orderPage(ModelAndView mav, HttpSession session) {
		if (session.getAttribute("user") != null) {
			// 로그인이 된 상태라면?
			if (session.getAttribute("shop") != null) {
				//가게 정보도 있는 상태라면?
				// 정상적으로 현재 주문을 보여 준다.
				
				//1. Order Status가 1인 Order를 가져온다.
				List<OrderViewVO> orders = orderService.getOrderList((String) session.getAttribute("shop"));
				
				List<List<DetailOrderViewVO>> finalList = new ArrayList<List<DetailOrderViewVO>>();
				
				// status가 1인 오더 중에서 DetailOrder 리스트를 가져온다
				for (OrderViewVO order : orders) {
					List<DetailOrderViewVO> items = new ArrayList<>();
					List<DetailOrderJoinVO> lst = detailOrderService.getDetailOrderList(order.getId());
					for (DetailOrderJoinVO item : lst) {
						DetailOrderViewVO vitem = new DetailOrderViewVO();
						vitem.setOrderId(order.getId());
						vitem.setShoptableName(order.getShoptableName());
						vitem.setProductName(item.getProductName());
						vitem.setQuantity(item.getQuantity());
						items.add(vitem);
					}
					finalList.add(items);
				}
				mav.addObject("orderList", finalList);
				mav.setViewName("order.jsp");
				
			} else {
				mav.addObject("msg", "매장 정보가 없습니다. 매장을 먼저 생성해주세요.");
				mav.setViewName("error.jsp");
			}
		} else {
			mav.setViewName("redirect:/login");
		}
		return mav;
	}
	
	// 상점 페이지 (관리자용)
	@RequestMapping(value="/order/close/{orderId}", method=RequestMethod.GET)
	public ModelAndView closeOrder(@PathVariable String orderId, ModelAndView mav, HttpSession session) {
		if (session.getAttribute("user") != null) {
			// 로그인이 된 상태라면?
			if (session.getAttribute("shop") != null) {
				OrderVO o = new OrderVO();
				o.setId(orderId);
				orderService.deleteOrder(o);
				
				mav.setViewName("redirect:/order");

			} else {
				mav.addObject("msg", "매장 정보가 없습니다. 매장을 먼저 생성해주세요.");
				mav.setViewName("error.jsp");
			}
		} else {
			mav.setViewName("redirect:/login");
		}
		return mav;
	}
	
	// 손님용 주문 페이지
	@RequestMapping(value = "/order/{shoptableId}", method = RequestMethod.GET)
	public ModelAndView customerOrderPage(@PathVariable String shoptableId, ModelAndView mav, HttpSession session) {
		// 1. 이 Shoptable이 유효한지 확인
		
		// 2. 유효하면 주문페이지로 이동
		ShopTableVO vo = new ShopTableVO();
		vo.setId(shoptableId);
		
		vo = shoptableService.getShopTable(vo);
		String shopId = vo.getShopId();
		
		List<ProductVO> products = productService.getProductList(shopId);
		
		mav.addObject("productList", products);
		mav.addObject("shopId", shopId);
		mav.addObject("shoptable", vo);
		mav.setViewName("/mobileorder.jsp");
		
		return mav;
	}
	
	// 손님용 주문 페이지
	@RequestMapping(value = "/order", method = RequestMethod.POST)
	public ModelAndView customerOrder(@RequestParam(value="shoptableId") String shoptableId, 
									  @RequestParam(value="shopId") String shopId,
									  @RequestParam(value="productId") String[] productIds,
									  ModelAndView mav, HttpSession session) {
		
//		for (String d : productIds) {
//			System.out.println(d);
//			if (!d.equals("")) {
//				break;
//			} else {
//				mav.setViewName("redirect:/order/" + shoptableId);
//				return mav;
//			}
//		}
		
		OrderVO order = new OrderVO();
		String orderId = UUID.randomUUID().toString().replace("-", "");
		order.setId(orderId);
		order.setShopId(shopId);
		order.setShoptableId(shoptableId);
		order.setStatus(1);
		order.setTxId("null");
		order.setCreatedAt();
		orderService.insertOrder(order);
		
		System.out.println(shoptableId);
		System.out.println(shopId);
		
		for (String x : productIds) {
			if (!x.equals("")) {
				DetailOrderVO dorder = new DetailOrderVO();
				dorder.setId(UUID.randomUUID().toString().replace("-", ""));
				dorder.setOrderId(orderId);
				dorder.setPrice(0);
				dorder.setProductId(x.substring(0, x.indexOf('_')));
				dorder.setQuantity(Integer.parseInt(x.substring(x.indexOf('_') + 1)));
				detailOrderService.insertDetailOrder(dorder);
			}
		}
		
		msgtmp.setMessageConverter(new StringMessageConverter());
		msgtmp.convertAndSend("/subscribe/notice", "HELLO");

		// 2. 유효하면 주문페이지로 이동
		mav.setViewName("/thankyou.jsp");
		return mav;
	}

	

}
