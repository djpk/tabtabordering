package com.deadycat.tabtab.view.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.deadycat.tabtab.shop.ShopVO;
import com.deadycat.tabtab.shop.impl.ShopService;
import com.deadycat.tabtab.user.UserVO;
import com.deadycat.tabtab.user.impl.UserService;

@Controller
public class LoginController {
	@Autowired
	private UserService userService;
	@Autowired
	private ShopService shopService;
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public ModelAndView loginPage(UserVO vo, ModelAndView mav, HttpSession session) {
		if (session.getAttribute("user") != null) {
			System.out.println("이미로그인대잇어" + session.getAttribute("user"));
			return null;
		} else {
			mav.setViewName("login.jsp");
		}
		return mav;
	}

	@RequestMapping(value="/login", method=RequestMethod.POST)
	public ModelAndView login(UserVO vo, ModelAndView mav, HttpSession session) { // 매개변수로 선언하면 Spring 컨테이너가 알아서 객체 생성해줌
		UserVO user = userService.getUser(vo); // userDAO는 생성됐는데 안에 Autowired가 작동을 안함
		
		if (!user.getPassword().equals(vo.getPassword())) {
			mav.setViewName("redirect:/login");
			return mav;
		}
		
		ShopVO shop = null;
		
		try {
			shop = shopService.getShopByUserId(vo.getId());
		} catch (Exception e) {
			shop = null;
		}
		
		if (shop != null) {
			session.setAttribute("shop", shop.getId());
			System.out.println("가게 정보 : " + shop.getId());
		} else {
			System.out.println("등록된 가게 정보가 없습니다.");
		}
		
		if (user != null) {
			session.setAttribute("user", vo.getId());
			
			mav.setViewName("redirect:/order");
		} else {
			mav.setViewName("redirect:/login");
		}
		return mav;
	}
	
	@RequestMapping(value="/signup", method=RequestMethod.GET)
	public ModelAndView signupPage(UserVO vo, ModelAndView mav, HttpSession session) {
		if (session.getAttribute("user") != null) {
			System.out.println("이미로그인되어있습니다." + session.getAttribute("user"));
			mav.setViewName("page.jsp");
		} else {
			mav.setViewName("signup.jsp");
		}
		return mav;
	}
	
	@RequestMapping(value="/signup", method=RequestMethod.POST)
	public ModelAndView signup(UserVO vo, ModelAndView mav, HttpSession session) { // 매개변수로 선언하면 Spring 컨테이너가 알아서 객체 생성해줌
		vo.setLoginId("TEST PURPOSE");
		vo.setCreatedAt();
		userService.insertUser(vo);
		
		mav.setViewName("login.jsp");
		
		return mav;
	}
	
	@RequestMapping(value="/logout", method=RequestMethod.GET)
	public ModelAndView logout(ModelAndView mav, HttpSession session) {
		session.invalidate();
		mav.setViewName("login.jsp");
		return mav;
	}

}
