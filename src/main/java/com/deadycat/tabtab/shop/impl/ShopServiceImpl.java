package com.deadycat.tabtab.shop.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deadycat.tabtab.shop.ShopVO;

@Service("shopService")
public class ShopServiceImpl implements ShopService {
	@Autowired
	private ShopDAO shopDAO;
	
	// Setter 인젝션 처리
	public void setShopDAO(ShopDAO shopDAO) {
		this.shopDAO = shopDAO;
	}
	
	public void insertShop(ShopVO vo) {
		// TODO Auto-generated method stub
		shopDAO.insertShop(vo);
	}

	public void updateShop(ShopVO vo) {
		// TODO Auto-generated method stub
		shopDAO.updateShop(vo);
		
	}

	public void deleteShop(ShopVO vo) {
		// TODO Auto-generated method stub
		shopDAO.deleteShop(vo);
	}

	public ShopVO getShop(ShopVO vo) {
		// TODO Auto-generated method stub
		return shopDAO.getShop(vo);
	}

	public List<ShopVO> getShopList(ShopVO vo) {
		// TODO Auto-generated method stub
		return shopDAO.getShopList(vo);
	}

	@Override
	public ShopVO getShopByUserId(String id) {
		// TODO Auto-generated method stub
		return shopDAO.getShopByUserId(id);
	}

}
