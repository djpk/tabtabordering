package com.deadycat.tabtab.shop.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.deadycat.tabtab.shop.ShopVO;

// ShopDAO는 정상적인 <bean> 등록으로 객체 생성할 예정이므로, 어노테이션은 설정하지 않는다.
@Repository("shopDAO")
public class ShopDAO implements ShopService {
		@Autowired
		private JdbcTemplate jdbcTemplate;
		
		// SQL 명령어들
		private final String SHOP_INSERT = "INSERT INTO shop(id, name, user_id, created_at) VALUES (?, ?, ?, ?)";
		//private final String USER_UPDATE = UPDATE user SET ;
		private final String SHOP_DELETE = "DELETE shop WHERE id = ?";
		private final String SHOP_GET = "SELECT * FROM shop where id = ?";
		//private final String SHOP_LIST = "SELECT * FROM shop ORDER BY id DESC";
		private final String SHOP_GET_BY_USERID = "SELECT * FROM shop where user_id = ?";

		// CRUD 기능의 메소드 구현
		// 숍 등록
		public void insertShop(ShopVO vo) {
			System.out.println("===> JDBC로 insertShop() 처리");
			jdbcTemplate.update(SHOP_INSERT, vo.getId(), vo.getName(), vo.getUserId(), vo.getCreatedAt());
		}
		
		// 숍 수정
		public void updateShop(ShopVO vo) {
			System.out.println("===> JDBC로 updateShop() 처리 ");
		}
		
		// 숍 삭제
		public void deleteShop(ShopVO vo) {
			System.out.println("===> JDBC로 deleteShop() 처리");
			jdbcTemplate.update(SHOP_DELETE, vo.getId());
		}
		
		// 숍 조회
		public ShopVO getShop(ShopVO vo) {
			System.out.println("===> JDBC로 getShop() 처리");
			return jdbcTemplate.queryForObject(SHOP_GET, new Object[] {vo.getId()},
					(rs, rowNum) -> new ShopVO(rs.getString("id"), rs.getString("name"), rs.getString("user_id"), rs.getString("created_at")));
		}
		
		
		// 숍 목록 조회
		public List<ShopVO> getShopList(ShopVO vo) {
			System.out.println("===> JDBC로 getShopList() 처리");
			return null;
		}

		@Override
		public ShopVO getShopByUserId(String id) {
			System.out.println("===> JDBC로 getShopById() 처리");
			return jdbcTemplate.queryForObject(SHOP_GET_BY_USERID, new Object[] {id},
					(rs, rowNum) -> new ShopVO(rs.getString("id"), rs.getString("name"), rs.getString("user_id"), rs.getString("created_at")));
		}
}
