package com.deadycat.tabtab.shop.impl;

import java.util.List;

import com.deadycat.tabtab.shop.ShopVO;

public interface ShopService {

	// CRUD 기능의 메소드 구현
	// 숍 등록
	void insertShop(ShopVO vo);

	// 숍 수정
	void updateShop(ShopVO vo);

	// 숍 삭제
	void deleteShop(ShopVO vo);

	// 숍 조회
	ShopVO getShop(ShopVO vo);
	
	ShopVO getShopByUserId(String id);

	// 숍 목록 조회
	List<ShopVO> getShopList(ShopVO vo);

}