package com.deadycat.tabtab.shop;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ShopVO {
	private String id;
	private String name;
	private String userId;
	private LocalDateTime createdAt;
	public ShopVO() { }
	
	public ShopVO(String id, String name, String userId, String createdAt) {
		super();
		this.id = id;
		this.name = name;
		this.userId = userId;
		this.setCreatedAt(createdAt);
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getCreatedAt() {
		return createdAt.toString();
	}
	public void setCreatedAt() {
		this.createdAt = LocalDateTime.now();
	}
	public void setCreatedAt(String time) {
		this.createdAt = LocalDateTime.parse(time, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
	}

	@Override
	public String toString() {
		return "ShopVO [id=" + id + ", name=" + name + ", userId=" + userId + ", created_at=" + createdAt + "]";
	}
	
}
