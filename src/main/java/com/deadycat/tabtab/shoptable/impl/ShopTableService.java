package com.deadycat.tabtab.shoptable.impl;

import java.util.List;

import com.deadycat.tabtab.shoptable.ShopTableVO;

public interface ShopTableService {

	// CRUD 기능의 메소드 구현
	// 숍테이블 등록
	void insertShopTable(ShopTableVO vo);

	// 숍테이블 수정
	void updateShopTable(ShopTableVO vo);

	// 숍테이블 삭제
	void deleteShopTable(ShopTableVO vo);

	// 숍테이블 조회
	ShopTableVO getShopTable(ShopTableVO vo);

	// 숍테이블 목록 조회
	List<ShopTableVO> getShopTableList(String shopId);

}