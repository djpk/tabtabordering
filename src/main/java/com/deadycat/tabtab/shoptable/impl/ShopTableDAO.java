package com.deadycat.tabtab.shoptable.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.deadycat.tabtab.shoptable.ShopTableVO;

@Repository("shoptableDAO")
public class ShopTableDAO implements ShopTableService {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	// SQL 명령어들
	private final String SHOPTABLE_INSERT = "INSERT INTO shoptable(id, name, pos_x, pos_y, width, height, shop_id, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
	//private final String USER_UPDATE = UPDATE user SET ;
	private final String SHOPTABLE_DELETE = "UPDATE shoptable SET status = 0 WHERE id = ?";
	private final String SHOPTABLE_GET = "SELECT * FROM shoptable where id = ?";
	private final String SHOPTABLE_LIST = "SELECT * FROM `shoptable` WHERE shop_id = ? AND status = 1 ORDER BY name ASC";

	// CRUD 기능의 메소드 구현
	// 숍테이블 등록
	public void insertShopTable(ShopTableVO vo) {
		System.out.println("===> JDBC로 insertShopTable() 처리");
		jdbcTemplate.update(SHOPTABLE_INSERT, vo.getId(), vo.getName(), vo.getPosX(), vo.getPosY(), vo.getWidth(), vo.getHeight(), vo.getShopId(), vo.getStatus());
	}

	// 숍테이블 수정
	public void updateShopTable(ShopTableVO vo) {
		System.out.println("===> JDBC로 updateShopTable() 처리 ");
	}

	// 숍테이블 삭제
	public void deleteShopTable(ShopTableVO vo) {
		System.out.println("===> JDBC로 deleteShopTable() 처리");
		jdbcTemplate.update(SHOPTABLE_DELETE, vo.getId());
	}

	// 숍테이블 조회
	public ShopTableVO getShopTable(ShopTableVO vo) {
		System.out.println("===> JDBC로 getShopTable() 처리");
		return jdbcTemplate.queryForObject(SHOPTABLE_GET, new Object[] {vo.getId()},
				(rs, rowNum) -> new ShopTableVO(rs.getString("id"), rs.getString("name"), rs.getInt("pos_x"), rs.getInt("pos_y"), rs.getInt("width"), rs.getInt("height"), rs.getString("shop_id"), rs.getInt("status")));
	}

	// 숍테이블 목록 조회
	public List<ShopTableVO> getShopTableList(String shopId) {
		System.out.println("===> JDBC로 getShopTableList() 처리");
		return jdbcTemplate.query(SHOPTABLE_LIST, new Object[] {shopId},
				(rs, rowNum) -> new ShopTableVO(rs.getString("id"), rs.getString("name"), rs.getInt("pos_x"), rs.getInt("pos_y"), rs.getInt("width"), rs.getInt("height"), rs.getString("shop_id"), rs.getInt("status")));
	}
}
