package com.deadycat.tabtab.shoptable.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deadycat.tabtab.shoptable.ShopTableVO;

@Service("shoptableService")
public class ShopTableServiceImpl implements ShopTableService {
	@Autowired
	private ShopTableDAO shopTableDAO;
	
	public void insertShopTable(ShopTableVO vo) {
		// TODO Auto-generated method stub
		shopTableDAO.insertShopTable(vo);
	}

	public void updateShopTable(ShopTableVO vo) {
		// TODO Auto-generated method stub
		shopTableDAO.updateShopTable(vo);
	}

	public void deleteShopTable(ShopTableVO vo) {
		// TODO Auto-generated method stub
		shopTableDAO.deleteShopTable(vo);
	}

	public ShopTableVO getShopTable(ShopTableVO vo) {
		// TODO Auto-generated method stub
		return shopTableDAO.getShopTable(vo);
	}

	public List<ShopTableVO> getShopTableList(String shopId) {
		// TODO Auto-generated method stub
		return shopTableDAO.getShopTableList(shopId);
	}

}
