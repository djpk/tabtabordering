package com.deadycat.tabtab.shoptable;

public class ShopTableVO {
	private String id;
	private String name;
	private int posX;
	private int posY;
	private int width;
	private int height;
	private String shopId;
	private int status;
	
	public ShopTableVO() { }
	public ShopTableVO(String id, String name, int posX, int posY, int width, int height, String shopId, int status) {
		super();
		this.id = id;
		this.name = name;
		this.posX = posX;
		this.posY = posY;
		this.width = width;
		this.height = height;
		this.shopId = shopId;
		this.status = status;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPosX() {
		return posX;
	}
	public void setPosX(int posX) {
		this.posX = posX;
	}
	public int getPosY() {
		return posY;
	}
	public void setPosY(int posY) {
		this.posY = posY;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public String getShopId() {
		return shopId;
	}
	public void setShopId(String shopId) {
		this.shopId = shopId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "ShopTableVO [id=" + id + ", name=" + name + ", posX=" + posX + ", posY=" + posY + ", width=" + width
				+ ", height=" + height + ", shopId=" + shopId + "]";
	}
}
