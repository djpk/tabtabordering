<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <title>회원 가입 - TABTAB.SHOP</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="/tabtab/css/login/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/tabtab/css/login/animate.css">
    <link rel="stylesheet" type="text/css" href="/tabtab/css/login/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/tabtab/css/login/login.css">
    <link rel="stylesheet" type="text/css" href="/tabtab/css/login/theme.css">

    <script type="text/javascript" src="/tabtab/js/login/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="/tabtab/js/login/bootstrap.min.js"></script>
</head>

<body>

    <div class="container">
        <div class="login-box">
                <div class="title"><h3>계정 생성</h3></div>
                <div class="box">
                <form id="login-form" action="signup" method="POST">
                    <div class="control">
                        <div class="label">아이디</div>
                        <input name="id" type="text" class="form-control" value="${userVO.id }" />
                    </div>
                    <div class="control">
                        <div class="label">비밀번호</div>
                        <input name="password" type="password" class="form-control" value="${userVO.password }" />
                    </div>
                    <div class="control">
                        <div class="label">이름</div>
                        <input name="name" type="text" class="form-control" value="${userVO.name }" />
                    </div>
                    <div class="control">
                        <div class="label">이메일 주소</div>
                        <input name="email" type="text" class="form-control" value="${userVO.email }" />
                    </div>
                    <div class="login-button">
                        <input type="submit" class="btn btn-orange" value="회원 가입"></button>
                    </div>
                </form>
                </div>
                <div class="info-box">
                   <span class="text-left"><a href="login.html">로그인 화면으로 돌아가기</a></span>
                   <div class="clear-both"></div>
                </div>
        </div>
    </div>
</body>

</html>
