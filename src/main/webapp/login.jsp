<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <title>TABTAB.SHOP</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" type="text/css" href="css/login/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/login/animate.css">
    <link rel="stylesheet" type="text/css" href="css/login/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/login/login.css">
    <link rel="stylesheet" type="text/css" href="css/login/theme.css">

    <script type="text/javascript" src="js/login/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="js/login/bootstrap.min.js"></script>
</head>

<body>
    <div class="container">
        <div class="login-box">
                <div class="title"><h3>로그인</h3></div>
                <div class="box">
                    <form id="login-form" action="login" method="post">
                        <div class="control">
                            <div class="label">아이디</div>
                            <input name="id" type="text" class="form-control" value="${userVO.id }" />
                        </div>
                        <div class="control">
                            <div class="label">비밀번호</div>
                            <input name="password" type="password" class="form-control" value="${userVO.password }" />
                        </div>
                        <div class="login-button">
                            <input type="submit" class="btn btn-orange" value="로그인"></button>
                        </div>
                    </form>
                </div>
                <div class="info-box">
                   <span class="text-left"><a href="signup">계정 생성</a></span>
                   <div class="clear-both"></div>
                </div>
        </div>
    </div>
</body>

</html>
