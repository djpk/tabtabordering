<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="top.jsp" %>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">선택한 상품의 가격과 가중치를 변경합니다.</h3>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <form action="/tabtab/product" method="POST">
                <input type="hidden" name="_method" value="put"/>
                <input type="hidden" name="productId" value="${productId }" />
				  <div class="form-group">
				    <label for="productPrice">가격</label>
				    <input type="text" class="form-control" id="productPrice" name="productPrice" placeholder="숫자만 입력하세요...">
				  </div>
				  <div class="form-group">
				    <label for="productWeight">가중치 (주문페이지에서 오름차순으로 정렬)</label>
				    <input type="text" class="form-control" id="productWeight" name="productWeight" placeholder="숫자만 입력하세요...">
				  </div>
				  <button type="submit" class="btn btn-primary">변경하기</button>
				</form>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                © TABTAB.SHOP
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
<%@ include file="bottom.jsp" %>