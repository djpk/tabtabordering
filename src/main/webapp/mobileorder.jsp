<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>주문 페이지</title>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="/tabtab/css/bootstrap.min.css" />

	<!-- Custom stlylesheet -->
	<link type="text/css" rel="stylesheet" href="/tabtab/css/order_style.css" />

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

</head>

<body>
	<div id="booking" class="section">
		<div class="section-center">
			<div class="container">
				<div class="row">
					<div class="booking-form">
						<form action="/tabtab/order" method="POST">
						<input type="hidden" name="shopId" value="${shopId }" />
                		<input type="hidden" name="shoptableId" value="${shoptable.id }" />
							<div class="form-header">
								<h2>${shoptable.name }</h2>
							</div>
							<c:forEach items="${productList }" var="product" varStatus="status">
							<div class="form-group">
								<select name="productId" class="form-control">
									<option value="" label="&nbsp;" selected hidden></option>
									<option value="">0</option>
                                    <option value="${product.id }_1">1</option>
									<option value="${product.id }_2">2</option>
									<option value="${product.id }_3">3</option>
                                    <option value="${product.id }_4">4</option>
                                    <option value="${product.id }_5">5</option>
								</select>
								<span class="select-arrow"></span>
								<span class="form-label">${product.name } ${product.price }원</span>
							</div>
							</c:forEach>
							<div class="form-btn">
								<button class="submit-btn">주문하기</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="/tabtab/js/jquery.min.js"></script>
	<script>
		$('.form-control').each(function () {
			floatedLabel($(this));
		});

		$('.form-control').on('input', function () {
			floatedLabel($(this));
		});

		function floatedLabel(input) {
			var $field = input.closest('.form-group');
			if (input.val()) {
				$field.addClass('input-not-empty');
			} else {
				$field.removeClass('input-not-empty');
			}
		}
	</script>
</body>
</html>