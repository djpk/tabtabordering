<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="top.jsp" %>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">상품 등록</h3>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <form action="product" method="POST">
				  <div class="form-group">
				    <label for="productName">상품 이름 (한 번 입력 후 수정 불가)</label>
				    <input type="text" class="form-control" id="productName" name="productName" placeholder="상품명을 입력하세요...">
				  </div>
				  <div class="form-group">
				    <label for="productPrice">가격</label>
				    <input type="text" class="form-control" id="productPrice" name="productPrice" placeholder="숫자만 입력하세요...">
				  </div>
				  <div class="form-group">
				    <label for="productWeight">가중치 (주문페이지에서 오름차순으로 정렬)</label>
				    <input type="text" class="form-control" id="productWeight" name="productWeight" placeholder="숫자만 입력하세요...">
				  </div>
				  <button type="submit" class="btn btn-primary">생성하기</button>
				</form>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                © TABTAB.SHOP
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
<%@ include file="bottom.jsp" %>