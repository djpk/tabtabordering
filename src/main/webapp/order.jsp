<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="top.jsp" %>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h3 class="text-themecolor">주문 목록</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">홈</a></li>
                            <li class="breadcrumb-item active">주문 목록</li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <!-- <h4 class="card-title">Basic Table</h4> -->
                                <!-- <h6 class="card-subtitle">Add class <code>.table</code></h6> -->
                                <div class="table">
								<c:forEach items="${orderList }" var="order" varStatus="status">
                                    <c:if test="${not empty order }">
                                    <div style="float: left">주문ID : ${order[0].orderId }</div>
                                    <div style="float: right"><a href="order/close/${order[0].orderId }" class="btn btn-success" role="button">주문 완결</a></div>
                                    <table class="table" style="margin-bottom: 5rem;">
                                        <thead>
                                            <tr>
                                            	<th>테이블명</th>
                                                <th>상품명</th>
                                                <th>수량</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                        	<c:forEach items="${order }" var="detailOrder" varStatus="status">
                                            <tr>
                                            	<td><c:out value="${detailOrder.shoptableName }"/></td>
                                                <td><c:out value="${detailOrder.productName }"/></td>
                                                <td><c:out value="${detailOrder.quantity }"/></td> 
                                            </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    </c:if>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer">
                © TABTAB.SHOP
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <script type="text/javascript" src="/tabtab/js/sockjs.min.js"></script>
		<script type="text/javascript" src="/tabtab/js/stomp.min.js"></script>
		<script type="text/javascript">
			var sock = null, stompClient = null;
			
			window.onload = function connect() {
				sock = new SockJS("http://13.209.229.109:8080/tabtab/detect");
		        stompClient = Stomp.over(sock);    //stomp client 구성
		        stompClient.connect({}, function(frame){
		            console.log('connected stomp over sockjs');
		            // subscribe message
		            stompClient.subscribe('/subscribe/notice', onMessage);
		        });
			}
			
			function onMessage(message){
			        console.log("Receive Data from server : "+message);
					location.reload(true);
			}
		</script>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
<%@ include file="bottom.jsp" %>